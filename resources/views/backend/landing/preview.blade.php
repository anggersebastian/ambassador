<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>GrapesJS Demo - Free Open Source Website Page Builder</title>
    <meta content="Best Free Open Source Responsive Websites Builder" name="description">
    <style>
        {{ $landing->css_content }}
    </style>
</head>
<body>
{!! $landing->html_content !!}
</body>
</html>